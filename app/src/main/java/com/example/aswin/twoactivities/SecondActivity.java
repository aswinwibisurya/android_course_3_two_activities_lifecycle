package com.example.aswin.twoactivities;

import androidx.appcompat.app.AppCompatActivity;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

public class SecondActivity extends AppCompatActivity {

    TextView tvMessage;
    EditText txtReply;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_second);

        Intent intent = getIntent();
        String message = intent.getStringExtra("message");

        tvMessage = findViewById(R.id.tvMessage);
        txtReply = findViewById(R.id.txtReply);

        tvMessage.setText(message);

        Log.d("SecondActivity", "onCreate called");
    }

    @Override
    protected void onStart() {
        super.onStart();
        Log.d("SecondActivity", "onStart called");
    }

    @Override
    protected void onResume() {
        super.onResume();
        Log.d("SecondActivity", "onResume called");
    }

    @Override
    protected void onPause() {
        super.onPause();
        Log.d("SecondActivity", "onPause called");
    }

    @Override
    protected void onStop() {
        super.onStop();
        Log.d("SecondActivity", "onStop called");
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        Log.d("SecondActivity", "onDestroy called");
    }

    public void sendReply(View view) {
        String reply = txtReply.getText().toString();

        Intent intent = new Intent();
        intent.putExtra("reply", reply);

        setResult(Activity.RESULT_OK, intent);

        finish();
    }
}
