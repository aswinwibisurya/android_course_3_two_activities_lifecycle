package com.example.aswin.twoactivities;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {

    public static final int REQUEST_CODE_REPLY = 1;
    EditText txtMessage;
    TextView tvReplyMessage;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        txtMessage = findViewById(R.id.txtMessage);
        tvReplyMessage = findViewById(R.id.tvReplyMessage);

        //load previously saved state (before rotation)
        if(savedInstanceState != null
                && savedInstanceState.containsKey("reply")) {
            String reply = savedInstanceState.getString("reply");
            tvReplyMessage.setText(reply);
        }

        Log.d("MainActivity", "onCreate called");
    }

    @Override
    protected void onStart() {
        super.onStart();
        Log.d("MainActivity", "onStart called");
    }

    @Override
    protected void onResume() {
        super.onResume();
        Log.d("MainActivity", "onResume called");
    }

    @Override
    protected void onPause() {
        super.onPause();
        Log.d("MainActivity", "onPause called");
    }

    @Override
    protected void onStop() {
        super.onStop();
        Log.d("MainActivity", "onStop called");
    }

    @Override
    protected void onSaveInstanceState(@NonNull Bundle outState) {
        outState.putString("reply", tvReplyMessage.getText().toString());
        super.onSaveInstanceState(outState);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        Log.d("MainActivity", "onDestroy called");
    }

    public void sendMessage(View view) {
        String message = txtMessage.getText().toString();

        Intent intent = new Intent(this, SecondActivity.class);
        intent.putExtra("message", message);

        //not expecting result
//        startActivity(intent);

        //expecting result
        startActivityForResult(intent, REQUEST_CODE_REPLY);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        if(requestCode == REQUEST_CODE_REPLY &&
            resultCode == Activity.RESULT_OK) {
            String reply = data.getStringExtra("reply");
            tvReplyMessage.setText(reply);
        }
        super.onActivityResult(requestCode, resultCode, data);
    }
}
